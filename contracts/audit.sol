pragma solidity ^0.4.24;

import "./SafeMath.sol";

contract newFactory
 {
    
    uint256 public money;// maxm amount set by client 
    address[] allEscrowContracts;
    
    uint256 public escrowCount; 
    
    constructor() public
     {
        escrowCount = 0;
     }
    
    function createContract(address caller, bytes32 ipfsAddress) public payable
    
    { 
        
         require(msg.value>0);
       
        money=msg.value;
        address newContract = new Escrow(caller, ipfsAddress, escrowCount++,money);
        allEscrowContracts.push(newContract);
        
    }

    function getAllContracts() public view returns (address[]) 
    {
        return allEscrowContracts;
    }
    
     
 }
  
// buyer is the client who writes the smart contract
// seller is the smart contract expert who audits the contract


contract Escrow
{
        mapping (address => uint256) private balances;
        
     
        address public escrowOwner;
        uint public escrowID;
        bytes32 ipfsAddress;
        bool status;
        address[] bidders;
        uint256[] public bidsArray;
        uint256 bidvalue;
        uint256 AmoutToTransfer;
        mapping (address => uint256) public bids;
        
    
    address bidWinner;
    
    
    constructor(address caller, bytes32 _ipfsAddress, uint256 _escrowID, uint256 _bidvalue) public 
    {
        
           escrowOwner = caller;
          ipfsAddress = _ipfsAddress;
         escrowID = _escrowID;
        bidvalue=_bidvalue;
    }
    
    function init_escrow() public
    {
        require(escrowOwner==msg.sender);
        status=true;
    }
    
    
    
    function bidToEscrow(uint256 bidAmount) public payable
    
    {   
        require(status=true);
        bids[msg.sender] = bidAmount;
        bidders.push(msg.sender);
        bidsArray.push(bidAmount);
        
    }
    
    function close_escrow() public
    {
        require(escrowOwner==msg.sender);
        status=false;
    }
    
    function select_bidder() public
    {
        require(escrowOwner==msg.sender);
        
        for(uint i = 0; i < bidders.length; i++)
        { uint256 minBid=bids[bidders[i]];
           
            if(bids[bidders[i]] > minBid)
            {
                minBid = bids[bidders[i]];
                bidWinner = bidders[i];
                AmoutToTransfer=bids[bidders[i]];
            }
    
        }
    
    }
    
    function showbidvalue() public view returns (uint256)
    {
        return AmoutToTransfer;
    }
    

   function winneris() public view returns (address) 
   {
        return bidWinner;
    } 
    

      function tranfer_funds() public
    { 
        require(bidvalue>=AmoutToTransfer);
        
        bidWinner.transfer(AmoutToTransfer);
        if(bidvalue>=AmoutToTransfer)
        
        {
        escrowOwner.transfer(bidvalue-AmoutToTransfer);
        }
    
        
    }
    
}